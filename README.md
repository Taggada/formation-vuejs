# Vue JS Formation 
## _Hey ! 👋  Retrouver ici la correction des exercices_

## Installation en local

Les exercices (exceptés les 3 premiers) ont besoin d'une version [Node.js](https://nodejs.org/) v10 ou supérieur, de  [Vuejs CLI](https://cli.vuejs.org/guide/installation.html)  et de la dernière version de [Yarn](https://yarnpkg.com/getting-started/install).

Pour lancer l'application en local.

```sh
cd formation-vuejs
yarn 
yarn run serve
```




